<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use App\Notifications\Email;
use Notification as NotificationFacade;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Notification\StoreRequest;

class NotificationsController extends ApiController
{
    public function notification(StoreRequest $request)
    {
        $notification = new Notification ();
        $notification->name = $request->name;
        $notification->phone = $request->phone;
        $notification->email = $request->email;
        $notification->subject = $request->subject;

        $notification->save();

        NotificationFacade::route('mail', $notification->email)
        ->notify(new Email($notification));

        $this->successResponse($notification,'Success');
    }
}
